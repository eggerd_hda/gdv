// 738730 - Dustin Eckhardt
// 738698 - Dennis Müßig

#include <iostream> 
#include <GL/freeglut.h> // alles von OpenGL
#include <GL/SOIL.h> // wird für Texturen benötigt

// Einstellungen
const float moving_floats_per_second = 0.005; // um wie viele Floats soll sich der Heli je Sekunde bewegen
const float wing_rotations_per_second = 25.0; // um wie viele Grad sollen sich die Rotoren je Sekunde drehen
const float body_rotations_per_second = 2.0; // um wie viel Grad soll sich der Heli je Sekunde drehen, wenn er am Rand ankommt
const float height_difference_per_second = 0.05; // um wie viele Floats soll sich der Heli in der Höhe bewegen
float maximal_movement_position = 0.60; // was ist die maximale X bzw. Y Koordinate bis zu der geflogen werden darf

// Animation-Values
float fPosX = 0.0;
float fPosY = 0.0;
float fPosZ = -1.0;
float fRotation_Wing = 0.0;
float fRotation_Body = 0.0;
bool auto_pilot = true;

// Texturen
GLuint tex_2d;


// Hier finden jene Aktionen statt, die zum Programmstart einmalig durchgeführt werden müssen
void Init()
{	
	// Licht
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	GLfloat light_position[] = { 1.0, 1.0, 1.0, 1.0 };
	glLightfv(GL_LIGHT0, GL_POSITION, light_position); // Licht Nr. 0 rechts oben
	glEnable(GL_COLOR_MATERIAL); // Farben der Objekte beibehalten

	// z-Buffer
	glEnable(GL_DEPTH_TEST);
	glClearDepth(1.0);

	// Normalen fuer korrekte Beleuchtungs-Berechnung normalisieren
	glEnable(GL_NORMALIZE);

	// Texture
	tex_2d = SOIL_load_OGL_texture("background.PNG", 
		SOIL_LOAD_AUTO, // Image laden, egal in welchem Format es gefunden wird
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y /* Image vertical spiegeln */ | SOIL_FLAG_NTSC_SAFE_RGB /* RGB Range 16-235 */ | SOIL_FLAG_COMPRESS_TO_DXT);
	glBindTexture(GL_TEXTURE_2D, tex_2d);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}


//Zeichenfunktion
void RenderScene()
{
	glClearColor(0.26, 0.41, 0, 1.0); // Hintergrundfarbe
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity(); // Aktuelle Model-/View-Transformations-Matrix zuruecksetzen
	gluLookAt(0., 0., 1., 0., 0., 0., 0., 1., 0.);

	glPushMatrix(); // Background für die Texture
	{
		glEnable(GL_TEXTURE_2D); // Texture setzen
		{
			glBegin(GL_QUADS);
			{
				glColor4f(1.0f, 1.0f, 1.0f, 1.0f); // Farbe für Fläche setzten - beeinflusst die Farbe der Texture, daher hier weiß (neutral)
				glTexCoord2f(0.0f, 0.0f); glVertex3f(-2.0f, -2.0f, -2.99f);
				glTexCoord2f(1.0f, 0.0f); glVertex3f(2.0f, -2.0f, -2.99f);
				glTexCoord2f(1.0f, 1.0f); glVertex3f(2.0f, 2.0f, -2.99f);
				glTexCoord2f(0.0f, 1.0f); glVertex3f(-2.0f, 2.0f, -2.99f);
			}
			glEnd();
		}
		glDisable(GL_TEXTURE_2D);
	}
	glPopMatrix();



	glTranslated(fPosX, fPosY, fPosZ); // aktuelle Position des Helis (wird dynamisch berechnet)

	glPushMatrix(); // Rumpf
	{
		glRotatef(fRotation_Body, 0., 0., 1.); // zum Ändern des Winkels, wenn in den Ecken angekommen
		glColor4f(0.94, 0.86, 0., 1.0); // Gelb

		glPushMatrix(); // Körper
		{
			glScalef(1.5, 1., 1.);
			glutSolidCube(0.2);
		}
		glPopMatrix();		

		glPushMatrix(); // Head
		{
			glTranslatef(0.2, 0., 0.);
			glScalef(1.5, 1.5, 1.);
			glutSolidCube(0.1);
		}
		glPopMatrix();

		glPushMatrix(); // Tail
		{
			glTranslatef(-0.25, 0., 0.);
			glScalef(2.5, 0.7, 0.7);
			glutSolidCube(0.1);
		}
		glPopMatrix();

		glPushMatrix(); // Tail-Tip
		{			
			glTranslatef(-0.382, 0., 0.035);
			glRotatef(-35, 0., 1., 0.);
			glScalef(0.7, 0.7, 1.2);			
			glutSolidCube(0.1);
		}
		glPopMatrix();

		glPushMatrix(); // Heckrotor
		{
			glTranslatef(-0.39, -0.04, 0.1); // dunkles Blau
			glRotatef(fRotation_Wing * -1, 0., 1., 0.); // rotieren mit dem Hauptrotor
			glColor4f(0.04, 0.09, 0.16, 1.0);

			glPushMatrix(); // Wing 1
			{
				glScalef(0.2, 0.1, 1.1);
				glutSolidCube(0.1);
			}
			glPopMatrix();

			glPushMatrix(); // Wing 2
			{				
				glScalef(1.1, 0.1, 0.2);
				glutSolidCube(0.1);
			}
			glPopMatrix();
		}
		glPopMatrix();

	}
	glPopMatrix();
	


	glPushMatrix(); // Main Rotor
	{
		glColor4f(0.04, 0.09, 0.16, 1.0); // dunkles Blau
		glTranslatef(0., 0., 0.11);
		glRotatef(fRotation_Wing * -1, 0., 0., 1.); // Rotation des Rotors (wird dynamisch berechnet)

		glPushMatrix(); // Wing 1
		{
			glScalef(2.5, 0.2, 0.1);
			glutSolidCube(0.2);
		}
		glPopMatrix();

		glPushMatrix(); // Wing 2
		{
			glScalef(0.2, 2.5, 0.1);
			glutSolidCube(0.2);
		}
		glPopMatrix();
	}
	glPopMatrix();

	glutSwapBuffers();
}


// Hier finden die Reaktionen auf eine Veränderung der Größe des Graphikfensters statt
void Reshape(int width, int height)
{
	glMatrixMode(GL_PROJECTION); // Matrix für Transformation: Frustum->viewport
	glLoadIdentity(); // Aktuelle Transformations-Matrix zuruecksetzen
	glViewport(0, 0, width, height); // Viewport definieren
	gluPerspective(45., 1., 0.01, 4.0); // Frustum definieren (siehe unten)
	glMatrixMode(GL_MODELVIEW); // Matrix für Modellierung/Viewing
}


// führt die Berechnungen zum Animieren der Objekte durch
void Animate(int value)
{	
	static int directionX = 1; // 1 = nach rechts fliegen; -1 = nach links fliegen // 0 = pausiert
	static int directionY = 1; // 1 = nach oben fliegen; -1 = nach unten fliegen

	// Rotationswinkel der Rotoblätter ändern
	fRotation_Wing = fRotation_Wing - wing_rotations_per_second;
	if (fRotation_Wing <= 0.0) 
	{
		fRotation_Wing = fRotation_Wing + 360.0;
	}


	if (auto_pilot == true)
	{
		// Ablauf um den Heli zu bewegen und zu drehen
		if (directionX == 1 && fPosX <= maximal_movement_position)  // es wird nach rechts geflogen, bis der Rand (0.25) erreicht wird
		{
			fPosX += moving_floats_per_second;
		}
		else
		{
			if (directionX != 0) // nur wenn X gerade nicht pausiert ist (verhindert Driften bei senkung der Höhe
			{
				directionX = -1; // später soll dann nach links geflogen werden
			}

			if (fRotation_Body < 90.0) // sind am Rand angekommen (ganz rechts); Heli um 90° drehen
			{
				fRotation_Body += body_rotations_per_second;
			}
			else
			{
				if (directionY == 1 && fPosY <= maximal_movement_position)  // es wird nach oben geflogen
				{
					fPosY += moving_floats_per_second;
				}
				else
				{
					directionY = -1; // später soll dann nach unten geflogen werden

					if (fRotation_Body < 180) // sind am Rand angekommen (ganz oben); Heli um 90° drehen
					{
						fRotation_Body += body_rotations_per_second;
					}
					else
					{
						if (directionX == -1 && fPosX >= -maximal_movement_position)  // es wird nach links geflogen
						{
							fPosX -= moving_floats_per_second;
						}
						else
						{
							directionX = 0; // X pausieren, um driften nach Links zu vermeiden, sollte die Höhe gesenkt werden

							if (fRotation_Body < 270) // sind am Rand angekommen (ganz links); Heli um 90° drehen
							{
								fRotation_Body += body_rotations_per_second;
							}
							else
							{
								if (directionY == -1 && fPosY >= -maximal_movement_position) // es wird nach unten geflogen
								{
									fPosY -= moving_floats_per_second;
								}
								else
								{
									if (fRotation_Body < 360) // sind am Rand angekommen (ganz unten); Heli um 90° drehen;
									{
										fRotation_Body += body_rotations_per_second;
									}
									else
									{
										fRotation_Body = 0; // Flag zurücksetzten, da wir 360° erreicht haben
										directionX = 1; // jetzt soll wieder nach rechts geflogen werden
										directionY = 1; // und anschließend wieder nach oben
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	glutPostRedisplay(); // RenderScene aufrufen
	int wait_msec = 10; // Timer wieder registrieren - Animate wird so nach 10 msec mit value+=1 aufgerufen.
	glutTimerFunc(wait_msec, Animate, ++value);
}


// Hänlder für Keyboard-Anschläge die ein ASCII Zeichen erzeugen
void KeyboardFunc(unsigned char key, int x, int y)
{
	if (key == 's') // wird "s" gedrückt - Höhe senken
	{
		if (fPosZ > -2.8) // bis zur maximalen Tiefe
		{
			fPosZ -= height_difference_per_second; // Höhe berechnen
			maximal_movement_position += height_difference_per_second * 0.4; // berechnet den neuen Rand
		}
	}

	if (key == 'w') // wird "w" gedrückt - Höhe steigern
	{
		if (fPosZ < 0.0) // bis zur maxmimalen Höhe
		{
			fPosZ += height_difference_per_second; // Höhe berechnen
			maximal_movement_position -= height_difference_per_second * 0.4; // berechnet den neuen Rand
		}
	}

	if (key == '+')
	{
		if (auto_pilot == true)
		{
			auto_pilot = false;
		}
		else
		{
			auto_pilot = true;
		}
	}

	// RenderScene aufrufen.
	glutPostRedisplay();
}


int main(int argc, char **argv)
{
	glutInit(&argc, argv); // GLUT initialisieren
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(700, 700); // Fenster-Konfiguration
	glutCreateWindow("Dustin Eckhardt; Dennis Müßig"); // Fenster-Erzeugung
	glutDisplayFunc(RenderScene); // Zeichenfunktion bekannt machen
	glutReshapeFunc(Reshape);
	// TimerCallback registrieren; wird nach 10 msec aufgerufen mit Parameter 0  
	glutTimerFunc(10, Animate, 0);
	glutKeyboardFunc(KeyboardFunc); // Händler für Keyboard-Events setzen
	Init();
	glutMainLoop();
	return 0;
}
