# Graphische Datenverarbeitung
*4\. Semester, 2016*  
*Prof. Elke Hergenröther & Björn Frömmer*

Unter dem Titel *"myMovingObject"* stand es uns frei ein beliebiges Objekt mit Hilfe von [OpenGL](https://www.opengl.org/) zu modellieren und zu animieren. Wir haben uns für einen simplistischen Helikopter entschieden, dessen Code in der Datei `/GDV/teil_e_v1.cpp` zu finden ist. Der Helikopter fliegt, wie gefordert, automatisch einen rechteckigen Kurs ab - die Flughöhe kann allerdings mit den Tasten `W` und `S` verändert werden.


## Behandelte Themen
- Das Verhalten von OpenGL beim Setzten von Farben
- Erzeugung von simplen geometrischen Flächen
- Ausrichten und Verschieben der Kamera
- Modifizieren der vorderen & hinteren Clipping-Ebene
- Zusammengesetzte Objekte transformieren (verschieben, skalieren, rotieren)
- Erstellen von Animationen


## Anforderungen
- Es soll eine 3D-Lösung erstellt werden, bei der ein beliebiges Objekt auf einer Bodenplatte einen rechteckigen Kurs abfährt und sich dabei an den Eckpunkten des Kurses jeweils um 90° dreht
- Für die 3D-Lösung soll ein zugehöriger Szenengraph erstellt werden, welcher wiederum aus mehreren Stufen und mehreren parallelen Ästen bestehen soll
- Die 3D-Lösung soll ein mehrteiliges Objekt enthalten, dessen Animation aus sich überlagernden Bewegungen besteht (z.B. vorwärts fliegender Vogel mit schlagenden Flügel)
- Fakultative Anforderungen für zusätzliche Bonuspunkte:
    - Lichtquellen & Schattenwurf
    - Verwendung von Texturen
    - Möglichkeit zur interaktiven Steuerung
    - Möglichkeit zum Umschalten zwischen verschiedenen Perspektiven


## Endergebnis
- Wir haben uns für einen Helikopter entschieden  
[![Helikopter GIF](https://gitlab.com/eggerd_hda/gdv/-/raw/assets/demo.gif)](https://gitlab.com/eggerd_hda/gdv/-/raw/assets/demo.mp4)
- Folgende fakultative Anforderungen wurden von uns erfüllt:
    - Licht & Schattenwurf (siehe Seitenwände des Helikopters)
    - Die Bodenplatte wurde mit einer Textur versehen
    - Die Flughöhe des Helikopters kann mit den Tasten `W` und `S` gesteuert werden
- Der zugehörige Szenengraph ([hier klicken um in neuen Tab zu öffnen, falls dieser nicht richtig dargestellt wird](https://gitlab.com/eggerd_hda/gdv/-/raw/assets/graph.png))
```mermaid
graph TD
    main((Helikopter))
    g1(( ))
    g2((Rumpf & Heckrotor))
    g3((Hauptrotor))
    g4(( ))
    g5((Heckrotor))
    g6((Rumpf))
    g7((Körper))
    g8((Kopf))
    g9((Tail))
    g10((Tail Tip))
    g11(( ))
    g12(( ))
    t1>Verschieben: aktuelle Position]
    t2>Drehen: in Blickrichtung]
    t3>Farbe: dunkel]
    t4>Verschieben: Ursprung]
    t5>Verschieben: z 0,11]
    t6>Farbe: Gelb]
    t7>Verschieben: X=-0,39 / Y=-0,04 / Z=0,1]
    t8>Drehen: Z]
    t9>Drehen: Y]
    t10>Skalieren: X=1,5]
    t11>Verschieben: X=0,2]
    t12>Verschieben: X=-0,25]
    t13>Verschieben: X=-0,382 / Z=0,035]
    t14>Skalieren: X=1,5 / Y=1,5]
    t15>Skalieren: X= 2,5 / Y=0,7 / Z=0,7]
    t16>Drehen: Y=-35 Grad]
    t17>Farbe: dunkel]
    t18>Skalieren: X=2,5 / Y=0,2 / Z=0,1]
    t19>Skalieren: X=0,7 / Y=0,7 / Z=1,2]
    t20>Skalieren: X=0,2 / Y=2,5 / Z=0,1]
    t21>Skalieren: X=0,2 / Y=0,1 / Z=1,1]
    t22>Skalieren: X=1,1 / Y=0,1 / Z=0,2]
    o1[Solid Cube 0,1]
    o2[Solid Cube 0,2]
    
    main --- t1
    t1 --- g1    
        g1 --- g3
            g3 --- t3
            t3 --- t5
            t5 --- t8
            t8 --- g12
                g12 --- t18
                    t18 --- o2
                g12 --- t20
                    t20 --- o2
        g1 --- g2
            g2 --- t2
            t2 --- t4
            t4 --- g4
                g4 --- t6
                t6 --- g6    
                    g6 --- g7
                        g7 --- t10
                        t10 --- o2
                    g6 --- g8
                        g8 --- t11
                        t11 --- t14
                        t14 --- o1
                    g6 --- g9
                        g9 --- t12
                        t12 --- t15
                        t15 --- o1
                    g6 --- g10
                        g10 --- t13
                        t13 --- t16
                        t16 --- t19
                        t19 --- o1
                g4 --- g5
                    g5 --- t7
                    t7 --- t9
                    t9 --- t17
                    t17 --- g11
                        g11 --- t21
                            t21 --- o1
                        g11 --- t22
                            t22 --- o1        

    subgraph Legende
        info1((Gruppe))
        info2>Transformation]
        info3[Geometrie]
    end
```    


## Demo
Über den nachfolgenden Download-Link kann eine bereits kompilierte Version heruntergeladen werden. Nach dem entpacken des ZIP-Archivs einfach die `GDV.exe` ausführen, um die Demo zu starten. Mit den Tasten `W` und `S` kann die Flughöhe des Helikopters verändert werden.

- [Demo für Windows herunterladen](https://gitlab.com/eggerd_hda/gdv/-/raw/assets/demo.zip) (3,26 MB)


## Beteiligte Personen
- [Dennis Müßig](https://gitlab.com/demues)
- [Dustin Eckhardt](https://gitlab.com/eggerd)


## Verwendete Software
- [OpenGL](https://www.opengl.org/)
- [FreeGLUT](http://freeglut.sourceforge.net/) (3.0.0)